# gifski

Install gifski from [released DEB file](https://github.com/ImageOptim/gifski/releases)
using Cargo (the Rust cargo manager) on Ubuntu.

[My R Ansible role](https://codeberg.org/ansible/R) installs the
[R package gifski](https://cran.r-project.org/package=gifski),
which requires gifski (this role).

gifski is also required by [peek](https://github.com/phw/peek),
which currently is my gif screen recorder of choice.
